CC = gcc
BUILDFLAGS = -std=c17 -Wall -Wextra -g -O0 -ljson-c -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_mixer
RELEASEFLAGS = -std=c17 -O3 -ljson-c -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_mixer
SOURCEFILES = source/*.c

build:
	$(CC) $(BUILDFLAGS) $(SOURCEFILES)

run:
	./a.out

release:
	$(CC) $(RELEASEFLAGS) $(SOURCEFILES)


