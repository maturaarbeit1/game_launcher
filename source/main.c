#include<stdio.h>
#include<SDL2/SDL.h>
#include<SDL2/SDL_image.h>
#include<SDL2/SDL_ttf.h>
#include<SDL2/SDL_mixer.h>
#include<string.h>
#include<json-c/json.h>
#include<stdlib.h>
#include<stdbool.h>


#define MAX_NUM_GAMES 10
unsigned int num_games = MAX_NUM_GAMES;
int selected_game_idx = 0;
bool launcher_running = true;

float virtual_pixel_width = 1;
float virtual_pixel_height = 1;

int window_width;
int window_height;
SDL_Window* window = NULL;
SDL_Renderer* renderer = NULL;
TTF_Font* title_font = NULL;
Mix_Chunk* button_sound = NULL;
SDL_GameController* gamepad = NULL;

typedef struct game_entry
{
    char name[31];
    SDL_Texture* name_texture;
    char image_path[51];
    SDL_Texture* image_texture;
    char launch_cmd[51];
} game_entry;

typedef struct game_icon_pair
{
    SDL_Rect name_rect;
    SDL_Rect image_rect;

} game_icon_pair;


int read_in_game_entries(char* file, game_entry* game_entries);
int run_game(game_entry* game);

int load_textures(game_entry* game);
void free_textures(game_entry* game);


int draw_game_icon_image(game_entry* game_entry, const SDL_Rect* img_dstrect);
int draw_game_icon_text(game_entry* game_entry, const SDL_Rect* text_dstrect);

int main()
{
    // Setup
    SDL_Init(SDL_INIT_EVERYTHING);
    IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG);
    TTF_Init();
    Mix_Init(0);
    window = SDL_CreateWindow("Game Launcher", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 1, 1, SDL_WINDOW_FULLSCREEN_DESKTOP);
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 2048);

    SDL_GetWindowSizeInPixels(window, &window_width, &window_height);
    virtual_pixel_width = window_width / 1920.0f;
    virtual_pixel_height = window_height / 1080.0f;

    // games
    game_entry games[MAX_NUM_GAMES];
    read_in_game_entries("games.json", games);
    title_font = TTF_OpenFont("Assets/atarian-system_bold.ttf", 100);

    game_icon_pair game_icons[MAX_NUM_GAMES];
    for (unsigned int i = 0; i < num_games; i++)
    {
        game_icons[i].image_rect = (SDL_Rect){.x = 250 + 820 * i, .y = 260, .w = 600, .h = 600};
        game_icons[i].name_rect = (SDL_Rect){.x = game_icons[i].image_rect.x + 300, .y = 900, .w = strlen(games[i].name) * 70, .h = 100};
        game_icons[i].name_rect.x -= game_icons[i].name_rect.w / 2.0;
    }

    for (unsigned int i = 0; i < num_games; i++)
        load_textures(& games[i]);

    // title
    SDL_Surface* temp = TTF_RenderText_Blended(title_font, "Spieleauswahl", (SDL_Color){0xff, 0xff, 0xff, 0xff});
    SDL_Texture* title_texture = SDL_CreateTextureFromSurface(renderer, temp);
    SDL_FreeSurface(temp);
    SDL_Rect title_rect = {.x = (960 - 13 * 70 / 2) * virtual_pixel_width, .y = 50 * virtual_pixel_height, .w = (13 * 70) * virtual_pixel_width, .h = 100 * virtual_pixel_height};

    // Sounds
    button_sound = Mix_LoadWAV("Assets/button_select_sound.wav");

    // Controller


    SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xff);
    SDL_RenderClear(renderer);

    // actual program (loop)
    SDL_Event event;
    while(launcher_running)
    {
            // render
            SDL_RenderClear(renderer);
            // title
            SDL_RenderCopy(renderer, title_texture, NULL, &title_rect);

            // games
            for (unsigned int i = 0; i < num_games; i++)
            {
                draw_game_icon_image(& games[i], & game_icons[i].image_rect);
                if (i == (unsigned int)selected_game_idx)
                    draw_game_icon_text(& games[i], & game_icons[i].name_rect);
            }
            SDL_RenderPresent(renderer);

            // input
            SDL_WaitEvent(&event);
            switch(event.type)
            {
                case SDL_CONTROLLERBUTTONDOWN:
                    {
                        switch(event.cbutton.button)
                        {
                            case SDL_CONTROLLER_BUTTON_DPAD_RIGHT:
                                {
                                    Mix_PlayChannel(-1, button_sound, 0);
                                    selected_game_idx++;
                                    if (selected_game_idx > (int)num_games - 1)
                                        selected_game_idx = 0;
                                    break;
                                }
                            case SDL_CONTROLLER_BUTTON_DPAD_LEFT:
                                {
                                    Mix_PlayChannel(-1, button_sound, 0);
                                    selected_game_idx--;
                                    if (selected_game_idx < 0)
                                        selected_game_idx = (int)num_games - 1;
                                    break;
                                }
                            case SDL_CONTROLLER_BUTTON_B:
                                {
                                    if (selected_game_idx >= 0 && selected_game_idx < (int)num_games)
                                    {
                                        Mix_PlayChannel(-1, button_sound, 0);
                                        run_game(& games[selected_game_idx]);
                                    }
                                    break;
                                }
                            default:
                                break;
                        }

                        break;
                    }
                case SDL_KEYDOWN:
                    {
                        switch(event.key.keysym.scancode)
                        {
                            case SDL_SCANCODE_ESCAPE:
                                {
                                    launcher_running = false;
                                    break;
                                }
                            case SDL_SCANCODE_RIGHT:
                                {
                                    Mix_PlayChannel(-1, button_sound, 0);
                                    selected_game_idx++;
                                    if (selected_game_idx > (int)num_games - 1)
                                        selected_game_idx = 0;
                                    break;
                                }
                            case SDL_SCANCODE_LEFT:
                                {
                                    Mix_PlayChannel(-1, button_sound, 0);
                                    selected_game_idx--;
                                    if (selected_game_idx < 0)
                                        selected_game_idx = (int)num_games - 1;
                                    break;
                                }
                            case SDL_SCANCODE_RETURN:
                                {
                                    if (selected_game_idx >= 0 && selected_game_idx < (int)num_games)
                                    {
                                        Mix_PlayChannel(-1, button_sound, 0);
                                        run_game(& games[selected_game_idx]);
                                    }
                                    break;
                                }
                            default:
                                break;

                        }
                        break;
                    }
                case SDL_CONTROLLERDEVICEREMOVED:
                    {
                        if (SDL_GameControllerGetAttached(gamepad) == false)
                            SDL_GameControllerClose(gamepad);
                        break;
                    }
                case SDL_CONTROLLERDEVICEADDED:
                    {
                        if (SDL_GameControllerGetAttached(gamepad) == false)
                        {
                            gamepad = SDL_GameControllerOpen(0);
                        }

                        break;
                    }

            }

    }







    // clean up
    for (unsigned int i = 0; i < num_games; i++)
        free_textures(& games[i]);
    SDL_DestroyTexture(title_texture);
    Mix_FreeChunk(button_sound);

    TTF_CloseFont(title_font);
    SDL_DestroyRenderer(renderer); 
    Mix_CloseAudio();
    SDL_DestroyWindow(window);
    
    Mix_Quit();
    IMG_Quit(); 
    TTF_Quit();
    SDL_Quit();
    

    return 0;
}



int read_in_game_entries(char* file, game_entry* game_entries)
{
    if (file == NULL || game_entries == NULL)
        return -1;
    
    FILE* fp;
    char file_content[2024];

    struct json_object* parsed_json;
    struct json_object* games;
    struct json_object* game;
    struct json_object* name;
    struct json_object* image;
    struct json_object* launch_cmd;

    fp = fopen(file, "r");
    if (fp == NULL)
        return -1;
    fread(file_content, 2048, 1, fp);
    fclose(fp);
    
    parsed_json = json_tokener_parse(file_content);

    json_object_object_get_ex(parsed_json, "games", &games);
    
    num_games = json_object_array_length(games);
    for (unsigned int i = 0; i < num_games; i++)
    {
        // get game element at index i
        game = json_object_array_get_idx(games, i);

        // get individual attributes
        json_object_object_get_ex(game, "name", &name);
        json_object_object_get_ex(game, "image", &image);
        json_object_object_get_ex(game, "launch_cmd", &launch_cmd);
        
        // copy attributes to game entrie object
        strncpy(game_entries[i].name, json_object_get_string(name), 30);
        strncpy(game_entries[i].image_path, json_object_get_string(image), 50);
        strncpy(game_entries[i].launch_cmd, json_object_get_string(launch_cmd), 50);
        game_entries[i].name[30] = '\0';
        game_entries[i].image_path[50] = '\0';
        game_entries[i].launch_cmd[50] = '\0';
    }


    return 0;
}

int load_textures(game_entry* game)
{
    if (game == NULL)
        return -1;
    if (game->image_texture == NULL)
        SDL_DestroyTexture(game->image_texture);
    game->image_texture = IMG_LoadTexture(renderer, game->image_path);

    if (game->name_texture == NULL)
        SDL_DestroyTexture(game->name_texture);
    SDL_Surface* temp = TTF_RenderText_Blended(title_font, game->name, (SDL_Color){0xff, 0xff, 0xff, 0xff});
    game->name_texture = SDL_CreateTextureFromSurface(renderer, temp);
    return 0;
}

void free_textures(game_entry* game)
{
    if (game == NULL)
        return;
    if (game->name_texture != NULL)
        SDL_DestroyTexture(game->name_texture);
    if (game->image_texture != NULL)
        SDL_DestroyTexture(game->image_texture);
}

int run_game(game_entry* game)
{
    if (game == NULL)
        return -1;
    
    int status = system(game->launch_cmd);
    // Flush event loop
    SDL_PumpEvents();
    SDL_FlushEvents(0, -1);
    return status;
}



int draw_game_icon_image(game_entry* game_entry, const SDL_Rect* img_dstrect)
{
    if (game_entry == NULL)
        return -1;
    
    SDL_Rect effective_img_dstrect = {.x = img_dstrect->x * virtual_pixel_width, .y = img_dstrect->y * virtual_pixel_height, .w = img_dstrect->w * virtual_pixel_width, .h = img_dstrect->h * virtual_pixel_height};

    SDL_RenderCopy(renderer, game_entry->image_texture, NULL, &effective_img_dstrect);

    return 0;
}

int draw_game_icon_text(game_entry* game_entry, const SDL_Rect* text_dstrect)
{
    if (game_entry == NULL)
        return -1;

    SDL_Rect effective_text_dstrect = {.x = text_dstrect->x * virtual_pixel_width, .y = text_dstrect->y * virtual_pixel_height, .w = text_dstrect->w * virtual_pixel_width, .h = text_dstrect->h * virtual_pixel_height};
    SDL_RenderCopy(renderer, game_entry->name_texture, NULL, &effective_text_dstrect);

    return 0;

}


































